       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-EMP1.
       AUTHOR. PLAIIFAH.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT EMP-FILE ASSIGN TO "emp1.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD EMP-FILE .
       01 EMP-DETAILS.
           88 END-OF-EMP-FILE VALUE HIGH-VALUES .
           05 EMP-SSN PIC 9(9).
           05 EMP-NAME.
              07 EMP-SURNAME PIC X(15).
              07 EMP-FORENAME PIC X(10).
           05 EMP-DATE-OF-BIRTH.
              07 EMP-YOB PIC 9(4).
              07 EMP-MOB PIC 99.
              07 EMP-DOB PIC 99.
           05 EMP-GENDER PIC X.
       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT EMP-FILE 
           PERFORM UNTIL END-OF-EMP-FILE 
              READ EMP-FILE 
                 AT END
                   SET END-OF-EMP-FILE TO TRUE
              END-READ
              IF NOT END-OF-EMP-FILE THEN
                 DISPLAY "***********************************"
                 DISPLAY "SSN : " EMP-SSN 
                 DISPLAY "NAME : " EMP-SURNAME " " EMP-FORENAME
                 DISPLAY "DOB : " EMP-DOB "/" EMP-MOB "/" EMP-YOB 
                 DISPLAY "GENDER : " EMP-GENDER 
              END-IF 
           END-PERFORM 
           CLOSE EMP-FILE 

           GOBACK .
